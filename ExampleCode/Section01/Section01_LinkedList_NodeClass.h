#ifndef SECTION01_LINKEDLIST_NODECLASS_H
#define SECTION01_LINKEDLIST_NODECLASS_H

#include "Section01_node.h"

template<class T>
class LinkedList{
    
    public:
        // Template node class for the head 
        Node<T>* head;

        /************************************
         * Function Name: LinkedList<T>
         * Preconditions: none
         * Postconditions: none 
         * Constructor with a new node 
         * *********************************/
        LinkedList<T>()
        {
            // new Node<T>() creates a Node will a nullptr
            this->head = new Node<T>();
        }

        /************************************
         * Function Name: ~LinkedList<T>
         * Preconditions: none
         * Postconditions: none 
         * Destructor 
         * *********************************/
        ~LinkedList<T>()
        {
            Node<T>* current = head;
        
            while (current != 0)
            { 
                Node<T>* next = current->getNodeLink();
                delete current;
                current = next;
            }
        
            head = nullptr;
        }

        /************************************
         * Function Name: getHeadNode()
         * Preconditions: none
         * Postconditions: Node<T>* 
         * Returns the head node  
         * *********************************/        
        Node<T>* getHeadNode(){
            return head;
        }

        /************************************
         * Function Name: display()
         * Preconditions: none
         * Postconditions: none
         * Prints all the elements of the node   
         * *********************************/          
        void display(){
            Node<T>* temp;

            if (head == NULL)
            {
                return;
            }
        
            temp = head;
        
            while (temp != NULL)
            {
                std::cout << temp->getNodeData() << ", " ;
                temp = temp->getNodeLink();
            }
            std::cout << std::endl;
        }

        /************************************
         * Function Name: insert()
         * Preconditions: T 
         * Postconditions: none
         * Inserts the new element at the end of the linked list   
         * *********************************/          
        void insert(T value){
            if (head->getNodeLink() == NULL)
            {
                head = new Node<T>();
                tail = head;
                head->setNodeLink(tail);
                head->setNodeData(value);
            }
            else
            {
                tail->setNodeLink(new Node<T>());
                tail = tail->getNodeLink();
                tail->setNodeData(value);
                tail->setNodeLink(nullptr);
            }   
            length++;
        }

       /************************************
         * Function Name: createCycle()
         * Preconditions: none
         * Postconditions: none
         * Sets the tail of new List equal to the head  
         * *********************************/          
        void createCycle(){
            tail->setLinkValue(head->getLink());
        }

       /************************************
         * Function Name: setNodeTail
         * Preconditions: Node<T>*
         * Postconditions: none
         * Sets a new node tail  
         * *********************************/          
        void setNodeTail(Node<T>* temp){
            tail = temp;
        }

       /************************************
         * Function Name: removeNode
         * Preconditions: Node<T>*
         * Postconditions: none
         * Sets a new node tail  
         * *********************************/         
        void removeNode(Node<T>* node)
        {
            if(node->getNodeLink() != nullptr)
                node->setNodeLink(node->getNodeLink()->getNodeLink());
        }
    
    private:
        Node<T> *tail;
        size_t length = 0;
    
};

#endif
/**********************************************
* File: AVLAuthorTest.cpp
* Author: 
* Email: 
*  
**********************************************/
#include <iostream>
#include <string>
#include "AVLTree.h"

// Struct goes here
struct Author{
	string firstName;
	string lastName;
	Author(std::string firstName, std::string lastName) : firstName(firstName), lastName(lastName) {}
	bool operator<(const Author& rhs) const{
		
		if(lastName < rhs.lastName)
			return true;
		else if(lastName == rhs.lastName){
			if(firstName < rhs.firstName)
				return true; 
		}
		return false;
	}
	bool operator==(const Author &rhs) const{
		if(lastName != rhs.lastName)
			return false;
		else {
			if( firstName != rhs.firstName)
				return false;
		}
		return true;
	}
	friend std::ostream& operator<<(std::ostream& os, const Author& printAuth); 
};

std::ostream& operator<<(std::ostream& os, const Author& printAuth){
	os << printAuth.lastName<<", " << printAuth.firstName;
	return os;
}

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
*  
* This is the main driver program for the 
* AVLTree function with strings 
********************************************/
int main(int argc, char **argv)
{
     Author aut1("Emma", "Ascolese");
     Author aut2("Emma", "Ascolese");
     Author Bad("Smelly", "Silly");
     
     
     AVLTree<Author> avlAuthor;
     avlAuthor.insert(aut1);
     avlAuthor.insert(Bad);	
     avlAuthor.printTree();	
     avlAuthor.remove(Bad);
     avlAuthor.printTree();	

    return 0;
}

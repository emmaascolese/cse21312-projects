/******************
 * File Name: Week1.cpp
 * Author: Emma Ascolese
 * E-mail: eascoles@nd.edu
 * Implement a B-Tree
 * ****************/


#include <string> 
#include "BTree.h" 
#include <iostream>

//struct for data
struct Data{
	int key;
	int data; 
	//Data ( int key, int data ) : key(key), data(data) {}
	bool operator >(const Data& rhs) const{
		if (key > rhs.key)
			return true;
		else 
			return false;
	}
	bool operator ==(const Data& rhs) const{
		if (key == rhs.key)
			return true;
		else
			return false;
	}
	bool operator <(const Data& rhs) const{
		if (key < rhs.key)
			return true;
		else
			return false;
	}
};
/*******************
 * Function name: main
 * Pre-condtions: int and char
 * Post-conditions: int
 * This is where numbers are inserted into the B-Tree and other randomly generated numbers are looked for
 * *****************/

int main(int argc, char **argv){
	BTree<Data> tree(1);
	//Test Case 1 
	BTree<Data> emptyTree(1);
	Data emptydat;
	std::cout<< "looking for something in an empty tree returns null: " << emptyTree.search(emptydat) << std::endl;
	//Test Case 2 
	BTree<Data> testTree1(1);
	Data dat;
	dat.key = 1;
	dat.data = 1; 
	testTree1.insert(dat);
	BTreeNode<Data>* l = testTree1.search(dat);
	BTreeNode<Data> i= testTree1.getInfo(l);
	std::cout<< "looking for the key 1. Here is it's address " << l << " This is the value: " << (i.keys[i.findKey(dat)]).key << std::endl;
	//Test Case 3 
	BTree<Data> notFoundT(1);
	Data d, p;
	d.key=0; p.key=1;
	d.data=0; p.data=1;
	notFoundT.insert(d);
	std::cout<< "looking for a key that is not there. Search returns null: " << notFoundT.search(p) <<std::endl;
	int count = 0 ,key, data;
	srand(time(NULL));
	while(count<1000){ //while loop to add 1000 numbers`
		Data add;
		add.key = rand()%200; 
		add.data = rand()%200;
		tree.insert(add);
		count++;
	}
	for ( int i=0; i<10; i++){
		Data check;
		check.key= rand()%200;
		BTreeNode<Data>* location = tree.search(check); 
		if( location){
			BTreeNode<Data> info= tree.getInfo(location);
			Data d=info.keys[info.findKey(check)];
			std::cout << "The node with this value was found in the tree: "  << d.key <<std::endl;
		} 
	}
	return 0;
}
		

/***************************
 * File: AgeHash.cpp
 * Author: Emma Ascolese
 * Email: eascoles@nd.edu 
 *
 * shows the comparison of the input or ordered and unordered sets
 * ************************/

#include <map>
#include <unordered_map>
#include <iterator>
#include <string>
#include <iostream>

/*************************
 * Function name: main
 * Pre-conditions: int, char**
 * Post conditions: int 
 *
 * main driver
 * ***********************/

int main(int argc, char** argv){
	std::map<std::string, int> ageHashOrdered = {{"Matthew", 38}, {"Alfred", 78}, {"Roscoe", 32}, {"James", 38} };
	std::map<std::string, int>::iterator iterOR;
	
	std::cout<< "The ordered ages are " << std::endl;
	for (iterOR= ageHashOrdered.begin(); iterOR != ageHashOrdered.end(); iterOR++){
		std::cout<< iterOR -> first << " " << iterOR->second << std::endl;
	} 
	
	std::unordered_map<std::string, int> ageHashUnordered = {{"Matthew", 38}, {"Alfred", 78}, {"Roscoe", 32}, {"James", 38} };
	std::unordered_map<std::string, int>::iterator iterUn;

	std::cout<< "The unordered ages are: " << std::endl;
	for (iterUn= ageHashUnordered.begin(); iterUn != ageHashUnordered.end(); iterUn++){
		std::cout<< iterUn -> first << " " << iterUn->second << std::endl;
	} 
	std::cout<< "The Ordered Example: " << ageHashOrdered["Matthew"] << std::endl;
	std::cout<< "The Unordered Example: " << ageHashUnordered["Alfred"] << std::endl;
	return 0;
}
